GitPython
Pillow
PyYAML
androguard>=3.3.5
apache-libcloud>=0.14.1
asn1crypto
clint
defusedxml
oscrypto
paramiko
platformdirs
puremagic
python-vagrant
qrcode
requests!=2.11.0,!=2.12.2,!=2.18.0,>=2.5.2
ruamel.yaml<0.17.22,>=0.15
sdkmanager>=0.6.4
yamllint

[:python_version < "3.11"]
tomli>=1.1.0

[:sys_platform == "darwin"]
biplist
pycountry

[docs]
numpydoc
pydata_sphinx_theme
pydocstyle
sphinx

[optional]
biplist
pycountry

[test]
html5print
pyjks
