Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: fdroidserver
Upstream-Contact: F-Droid <admin@f-droid.org>
Source: https://gitlab.com/fdroid/fdroidserver

Files: *
Copyright: 2016, Blue Jay Wireless
           2015, Boris Kraut
           2010-2016, Ciaran Gultnieks <ciaran@ciarang.com>
           2013-2017, Daniel Martí <mvdan@mvdan.cc>
           2010-2011, David Sterry
           2021-2022, FC Stegerman <flx@obfusk.net>
           2013, Frans Gifford
           2013-2021, Hans-Christoph Steiner <hans@eds.org>
           2011, Henrik Tunedal
           2019-2021, Jochen Sprickerhof <git@jochen.sprickerhof.de>
           2011, John Sullivan
           2011, Michael Haas
           2017-2021, Michael Pöhn <michael.poehn@fsfe.org>
           Ricki Hirner (bitfire web engineering)
           2012, Robert Martinez
           2017-2018, Torsten Grote <t@grobox.de>
           2011, William Theaker
           2017-2021, mimi89999 <michel@lebihan.pl>
           2017, tobiasKaminsky <tobias@kaminsky.me>
License: AGPL-3+

Files: tests/get_android_tools_versions/android-sdk/patcher/*
       tests/source-files/com.anpmech.launcher/*
Copyright: 2015-2017, Hayai Software
           2016, The Android Open Source Project
           2018, The KeikaiLauncher Project
License: Apache-2

Files: fdroidserver/apksigcopier.py
Copyright: 2023, FC Stegerman <flx@obfusk.net>
License: GPL-3+

Files: tests/source-files/Zillode/*
       tests/triple-t-2/build/org.piwigo.android/app/src/main/java/*
Copyright: 2015, OpenSilk Productions LLC
           2016-2017, Piwigo Team http://piwigo.org
           2018, Raphael Mack http://www.raphael-mack.de
License: GPL-3+

Files: fdroidserver/asynchronousfilereader/*
Copyright: 2014, Stefaan Lippens
License: MIT

Files: tests/source-files/se.manyver/index.android.js
Copyright: 2018-2019, The Manyverse Authors
License: MPL-2

Files: fdroidserver/looseversion.py
Copyright: Guido van Rossum <guido@cwi.nl> and others.
License: Python-2

Files: debian/*
Copyright: public-domain
License: public-domain

License: AGPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 .
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

License: Apache-2
  On Debian GNU/Linux system you can find the complete text of the
  Apache-2.0 license in '/usr/share/common-licenses/Apache-2.0'

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 3 can be found in `/usr/share/common-licenses/GPL-3'.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

License: MPL-2
 The contents of this file are subject to the Mozilla Public License Version
 2.0 (the "License"); you may not use this file except in compliance with
 the License.
 .
 Software distributed under the License is distributed on an "AS IS" basis,
 WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 for the specific language governing rights and limitations under the
 License.
 .
 On Debian systems, the complete text of the Mozilla Public License Version
 2.0 can be found in /usr/share/common-licenses/MPL-2.0.

License: Python-2
 PYTHON SOFTWARE FOUNDATION LICENSE VERSION 2
 --------------------------------------------
 .
 1. This LICENSE AGREEMENT is between the Python Software Foundation
 ("PSF"), and the Individual or Organization ("Licensee") accessing and
 otherwise using this software ("Python") in source or binary form and
 its associated documentation.
 .
 2. Subject to the terms and conditions of this License Agreement, PSF hereby
 grants Licensee a nonexclusive, royalty-free, world-wide license to reproduce,
 analyze, test, perform and/or display publicly, prepare derivative works,
 distribute, and otherwise use Python alone or in any derivative version,
 provided, however, that PSF's License Agreement and PSF's notice of copyright,
 i.e., "Copyright (c) 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010,
 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022 Python Software Foundation;
 All Rights Reserved" are retained in Python alone or in any derivative version
 prepared by Licensee.
 .
 3. In the event Licensee prepares a derivative work that is based on
 or incorporates Python or any part thereof, and wants to make
 the derivative work available to others as provided herein, then
 Licensee hereby agrees to include in any such work a brief summary of
 the changes made to Python.
 .
 4. PSF is making Python available to Licensee on an "AS IS"
 basis.  PSF MAKES NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR
 IMPLIED.  BY WAY OF EXAMPLE, BUT NOT LIMITATION, PSF MAKES NO AND
 DISCLAIMS ANY REPRESENTATION OR WARRANTY OF MERCHANTABILITY OR FITNESS
 FOR ANY PARTICULAR PURPOSE OR THAT THE USE OF PYTHON WILL NOT
 INFRINGE ANY THIRD PARTY RIGHTS.
 .
 5. PSF SHALL NOT BE LIABLE TO LICENSEE OR ANY OTHER USERS OF PYTHON
 FOR ANY INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS AS
 A RESULT OF MODIFYING, DISTRIBUTING, OR OTHERWISE USING PYTHON,
 OR ANY DERIVATIVE THEREOF, EVEN IF ADVISED OF THE POSSIBILITY THEREOF.
 .
 6. This License Agreement will automatically terminate upon a material
 breach of its terms and conditions.
 .
 7. Nothing in this License Agreement shall be deemed to create any
 relationship of agency, partnership, or joint venture between PSF and
 Licensee.  This License Agreement does not grant permission to use PSF
 trademarks or trade name in a trademark sense to endorse or promote
 products or services of Licensee, or any third party.
 .
 8. By copying, installing or otherwise using Python, Licensee
 agrees to be bound by the terms and conditions of this License
 Agreement.

License: public-domain
 To the best of our knowledge, with the exceptions noted below or
 within the files themselves, the files that constitute PyCrypto are in
 the public domain. Most are distributed with the following notice:
 .
 The contents of this file are dedicated to the public domain. To
 the extent that dedication to the public domain is not available,
 everyone is granted a worldwide, perpetual, royalty-free,
 non-exclusive license to exercise all rights associated with the
 contents of this file for any purpose whatsoever.
 No rights are reserved.
